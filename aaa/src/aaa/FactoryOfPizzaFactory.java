package aaa;

public class FactoryOfPizzaFactory {
	
	public static Pizza createPizza(String pizza) {
		
		if ( "Cheese".equals(pizza) ) {
			CheesePizzaFactory cpf = new CheesePizzaFactory();
			CheesePizza cheesePizza = cpf.create();
			return cheesePizza;
		}else if ( "Potato".equals(pizza) ) {
			PotatoPizzaFactory ppf = new PotatoPizzaFactory();
			PotatoPizza potatoPizza = ppf.create();
			return potatoPizza;
		}else {
			System.out.println("No! We Don't Have That Type Of Pizza");
			return null;
		}
	}
	
}
