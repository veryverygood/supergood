package aaa;

public class Client {
	public static void main(String[] args) {
		
		PotatoPizzaFactory ppf = new PotatoPizzaFactory();
		CheesePizzaFactory cpf = new CheesePizzaFactory();
		
		CheesePizza cp = cpf.create();
		PotatoPizza pp = ppf.create();
		cp.draw();
		pp.draw();
		
		FactoryOfPizzaFactory popf = new FactoryOfPizzaFactory();
		CheesePizza ccp = (CheesePizza) popf.createPizza("Cheese");
		PotatoPizza ppp = (PotatoPizza) popf.createPizza("Potato");
		Pizza temp = popf.createPizza("Cheese");
		ccp.draw();
		ppp.draw();
		
		try {
			Pizza ssp = popf.createPizza("Salami");
			ssp.draw();
		} catch ( Exception e ) {
			e.printStackTrace();
		}
		
		System.out.println( temp instanceof Pizza);
		System.out.println( temp instanceof CheesePizza);
		System.out.println( temp instanceof PotatoPizza);
		
	}
}
